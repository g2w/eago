package main

import (
	"gitee.com/g2w/eago/apis"
	"gitee.com/g2w/eago/core/config"
	"gitee.com/g2w/eago/core/database"
	"gitee.com/g2w/eago/core/middleware"
	"github.com/gin-gonic/gin"
)

func main() {
	// 配置文件加载
	config.Init()
	// 链接数据库
	database.Conn()

	// 生产环境开启
	if config.AppConfig.GIN.Dev == false {
		gin.SetMode(gin.ReleaseMode)
	}

	// 创建空白服务器
	s := gin.New()

	// 全局中间件注入
	s.Use(middleware.Cors())
	s.Use(middleware.Logger2File())
	s.Use(middleware.Recovery())

	// 路由载入
	apis.LoadRouters(s)
	// 监听服务
	addr := config.AppConfig.GIN.Host
	port := config.AppConfig.GIN.Port
	_ = s.Run(addr + ":" + port)
}
