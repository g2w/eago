module gitee.com/g2w/eago

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.9
)
