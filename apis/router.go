package apis

import (
	"gitee.com/g2w/eago/app/handlers/api"
	"gitee.com/g2w/eago/app/handlers/back"
	"gitee.com/g2w/eago/app/websokets"
	"github.com/gin-gonic/gin"
)

func LoadRouters(router *gin.Engine) {

	// websocket
	ws := router.Group("socket")
	{
		ws.GET("/getCurrentTime", websokets.GetCurrentTime)
	}

	// 客户端
	a := router.Group("api")
	{
		// 用户
		seeker := a.Group("user")
		{
			// 登录
			seeker.POST("/Login", api.Login)
		}
	}

	// 后台管理
	b := router.Group("back")
	{
		// 管理员
		manager := b.Group("manager")
		{
			manager.POST("/loginCheck", back.LoginCheck)
		}
	}

}
