package EagoLog

import (
	"encoding/json"
	"fmt"
	"gitee.com/g2w/eago/core/config"
	"gitee.com/g2w/eago/core/utils/EagoFile"
	"gitee.com/g2w/eago/core/utils/EagoTime"
	"io"
	"log"
	"os"
	"time"
)

type DataType map[string]interface{}

func ExceptionLog2File(logFields DataType) {
	filePath := "logs/exception/" + EagoTime.NowYearStr() + "/" + EagoTime.NowMonthStr() + "/"
	fileName := filePath + EagoTime.NowDayStr() + ".log"
	if !EagoFile.ExistPath(filePath) {
		_ = os.MkdirAll(filePath, os.ModePerm)
	}
	if !EagoFile.ExistsFile(fileName) {
		_ = EagoFile.CreateFile(fileName)
	}
	f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
	}
	// 是否同时输出到控制台
	if config.AppConfig.Logger.Logger2Console {
		log.SetOutput(io.MultiWriter(os.Stderr, f))
	} else {
		log.SetOutput(f)
	}
	// 屏蔽默认前缀时间
	log.SetFlags(0)
	// 开始时间
	startTime := time.Now()
	logFields["start_time"] = EagoTime.DateTimeF4ByNow(startTime)
	// 转成json字符串
	logFieldJson, _ := json.Marshal(logFields)
	logFieldJsonString := string(logFieldJson)
	// 写日志行
	log.Println(logFieldJsonString)
}
