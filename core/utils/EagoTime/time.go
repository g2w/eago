package EagoTime

import (
	"gitee.com/g2w/eago/app/models"
	"strconv"
	"time"
)

const (
	f1 = "2006-01-02"
	f2 = "2006-01-02 15:04:05"
	f3 = "20060102"
	f4 = "20060102150405"
	f5 = "01"
	f6 = "2006"
	f7 = "02"
)

// 时区 上海
var cstSh, _ = time.LoadLocation("Asia/Shanghai")

func now() time.Time {
	return time.Now().In(cstSh)
}

func NowDate() string {
	return now().Format(f1)
}

func NowDateTimeStr() string {
	return now().Format(f2)
}

func NowDateTimeObjByInput(timeStr string) time.Time {
	t, _ := time.Parse(f2, timeStr)
	return t.In(cstSh)
}

func NowTimeStampUnix() int64 {
	t, _ := time.Parse(f2, NowDateTimeStr())
	return t.In(cstSh).Unix()
}

func DateByStrDateTime(dateTimeStr string) string {
	t, _ := time.Parse(f2, dateTimeStr)
	return t.In(cstSh).Format(f1)
}

func YearByStrDateTime(dateTimeStr string) int {
	t, _ := time.Parse(f2, dateTimeStr)
	return t.In(cstSh).Year()
}
func MonthByStrDateTime(dateTimeStr string) int {
	t, _ := time.Parse(f2, dateTimeStr)
	t.In(cstSh).Format(f5)
	tt, _ := strconv.Atoi(t.In(cstSh).Format(f5))
	return tt
}

func DayByStrDateTime(dateTimeStr string) int {
	t, _ := time.Parse(f2, dateTimeStr)
	return t.In(cstSh).Day()
}

func NowDateF3() string {
	return now().Format(f3)
}

func NowYearStr() string {
	return strconv.Itoa(now().Year())
}

func NowMonthStr() string {
	return now().Format(f5)
}

func NowDayStr() string {
	return strconv.Itoa(now().Day())
}

func DateTimeF4ByNow(now time.Time) string {
	return now.In(cstSh).Format(f2)
}

func ConvertListDatetime(toFormatTime time.Time) time.Time {
	targetTime := toFormatTime.In(cstSh).Format(f2)
	t, _ := time.Parse(f2, targetTime)
	return t
}

func Str2TimeNormal(dateTimeStr string) models.TimeNormal {
	t, _ := time.Parse(f2, dateTimeStr)
	var tt models.TimeNormal
	tt = models.TimeNormal{Time: t}
	return tt
}

func Str2DateTimeStr(dateTimeStr string) string {
	t, _ := time.Parse(time.RFC3339, dateTimeStr)
	return t.Format(f2)
}

func RFC3339ToCSTLayout(value string) string {
	return value[0:10] + " " + value[11:19]
}
