package database

import (
	"fmt"
	"gitee.com/g2w/eago/core/config"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"time"
)

var DbConn *gorm.DB
//var SqlDb *sql.DB

func Conn() {
	var err error
	// 数据库配置
	dbConf := config.AppConfig.Mysql
	// 参考 https://github.com/go-sql-driver/mysql#dsn-data-source-name 获取详情
	dsn := fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=%s&loc=%s",
		dbConf.User,
		dbConf.Password,
		dbConf.Address,
		dbConf.Port,
		dbConf.DbName,
		dbConf.CharSet,
		dbConf.ParseTime,
		dbConf.Loc,
	)
	DbConn, err = gorm.Open(mysql.New(mysql.Config{
		DSN:                       dsn,
		DefaultStringSize:         256,   // string 类型字段的默认长度
		DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false, // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{
		SkipDefaultTransaction: true,
		PrepareStmt: true,
		Logger:logger.Default.LogMode(logger.Error),
	})

	if err != nil {
		panic(err)
	}
	// 自动创建不存在的模型
	for n, m := range modelList {
		_ = DbConn.Set("gorm:table_options", "ENGINE=InnoDB comment '"+n+"'").AutoMigrate(m)
	}

	// =============== 开启连接池 ===============
	sqlDb, _ := DbConn.DB()
	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDb.SetMaxIdleConns(500)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDb.SetMaxOpenConns(2000)
	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	sqlDb.SetConnMaxLifetime(time.Hour)

	//fmt.Println(SqlDb.Stats())
}
