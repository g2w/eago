package config

import (
	cfg "gopkg.in/gcfg.v1"
)

var AppConfig = struct {
	APP struct {
		AppName string
		Debug bool
		BaseURL string
	}
	GIN struct {
		Host string
		Port string
		Dev  bool
	}
	Logger struct {
		Logger2Console bool
	}
	WEB struct {
		StaticDir string
	}
	Mysql struct {
		Address   string
		Port      int
		User      string
		Password  string
		DbName    string
		CharSet   string
		ParseTime string
		Loc       string
	}
	Aes struct{
		Key string
	}
	JWT struct{
		JwtSecret string
	}
}{}

func Init() {
	err := cfg.ReadFileInto(&AppConfig, "core/config/app.conf")
	if err != nil {
		panic(err)
	}
}
