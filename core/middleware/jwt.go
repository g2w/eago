package middleware

import (
	"gitee.com/g2w/eago/core/http/resp"
	"gitee.com/g2w/eago/core/utils/EagoJwt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func JWY() gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var msg string
		code, msg = resp.ParseStatusInfo(resp.SUCCESS)
		token := c.Request.Header.Get("Authorization")
		//fmt.Println(token)
		if token == "" {
			code, msg = resp.ParseStatusInfo(resp.ErrorAuthNoToken)
		} else {
			// 去掉：|Bearer |
			token = token[7:]
			//fmt.Println(token)
			claims, err := EagoJwt.ParseToken(token)
			if err != nil {
				code, msg = resp.ParseStatusInfo(resp.ErrorAuthCheckTokenFail)
			} else if time.Now().Unix() > claims.ExpiresAt {
				code, msg = resp.ParseStatusInfo(resp.ErrorAuthCheckTokenTimeout)
			}
		}
		//如果token验证不通过，直接终止程序，c.Abort()
		if code != resp.ParseStatusCode(resp.SUCCESS) {
			ResponseData := gin.H{"code": code, "msg": msg, "result": nil}
			// 返回错误信息
			c.JSON(http.StatusUnauthorized, ResponseData)
			//终止程序
			c.Abort()
			return
		}
		c.Next()
	}
}
