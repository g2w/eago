package middleware

import (
	"fmt"
	"gitee.com/g2w/eago/core/http/resp"
	"github.com/gin-gonic/gin"
	"runtime"
	"strconv"
)

// 提取当前运行时文件信息 名称 行数
func trace() []map[string]string {
	var pcs [32]uintptr
	n := runtime.Callers(5, pcs[:])
	var traceData []map[string]string
	for _, pc := range pcs[:n] {
		fn := runtime.FuncForPC(pc)
		file, line := fn.FileLine(pc)
		lineInfo := map[string]string{"file": file, "line": strconv.Itoa(line)}
		traceData = append(traceData, lineInfo)
		// @todo 只记录打印错误最近的一行信息 break 了
		//break
	}
	return traceData
}

// 请求异常处理
func Recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				// 异常处理
				switch err.(type) {
				// 接管自定义的一些异常抛出
				case resp.HttpStatus:
					httpErr := err.(resp.HttpStatus)
					// fmt.Println(httpErr)
					// 参数缺失需要特别提示
					missInfo, isParamsMiss := c.Get("ParamsMissInfo")
					msg := httpErr[1].(string)
					var rlt map[string]interface{}
					if isParamsMiss {
						msg += " : " + missInfo.(string)
						pData, _ := c.Get("PostJsonData")
						gParams, _ := c.Get("GetParamData")
						rlt = map[string]interface{}{
							"get_param": gParams,
							"post_json": pData,
						}
					}
					ResponseData := gin.H{
						"code":   httpErr[0],
						"msg":    msg,
						"result": rlt,
					}
					// 相应数据添加到上下文 给后续中间件使用
					c.Set("ResponseData", ResponseData)
					c.JSON(200, ResponseData)
				default:
					message := fmt.Sprintf("%s", err)
					// 提取整理一下
					traceData := trace()
					position := traceData[0]["file"] + ":" + traceData[0]["line"]
					ResponseData := gin.H{
						"code": 5000,
						"msg":  "Internal Server Error ",
						"result": map[string]interface{}{
							"error_info": message,
							"position":   position,
							"detail":     trace(),
						},
					}
					c.Set("ResponseData", ResponseData)
					// fmt.Println(message)
					c.JSON(500, ResponseData)
				}
			}
			if c.Writer.Status() == 404 {
				ResponseData := gin.H{
					"code":   4004,
					"msg":    "404 api not found",
					"result": nil,
				}
				c.Set("ResponseData", ResponseData)
				c.JSON(404, ResponseData)
			}

		}()

		c.Next()
	}
}
