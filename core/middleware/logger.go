package middleware

import (
	"encoding/json"
	"fmt"
	"gitee.com/g2w/eago/core/config"
	"gitee.com/g2w/eago/core/utils/EagoFile"
	"gitee.com/g2w/eago/core/utils/EagoTime"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

// 请求日志输入到文件
func Logger2File() gin.HandlerFunc {
	return func(context *gin.Context) {
		// 开始时间
		startTime := time.Now()
		//context.Set("cid", startTime.Format("2006150405.0000:")+fmt.Sprintf("%p", &context))
		// 处理请求
		context.Next()
		// 结束时间
		endTime := time.Now()

		// 异步处理log到文件
		go func(context *gin.Context, startTime time.Time, endTime time.Time) {
			// 执行时间
			sf, _ := strconv.ParseFloat(startTime.Format("150405.0000"), 64)
			ef, _ := strconv.ParseFloat(endTime.Format("150405.0000"), 64)
			latencyTime := ef - sf

			filePath := "logs/access/" + EagoTime.NowYearStr() + "/" + EagoTime.NowMonthStr() + "/"
			fileName := filePath + EagoTime.NowDayStr() + ".log"
			if !EagoFile.ExistPath(filePath) {
				_ = os.MkdirAll(filePath, os.ModePerm)
			}
			if !EagoFile.ExistsFile(fileName) {
				_ = EagoFile.CreateFile(fileName)
			}
			f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				fmt.Println(err)
			}
			// 是否同时输出到控制台
			if config.AppConfig.Logger.Logger2Console {
				log.SetOutput(io.MultiWriter(os.Stderr, f))
			} else {
				log.SetOutput(f)
			}
			// 屏蔽默认前缀时间
			log.SetFlags(0)
			//log.SetPrefix("ers:")
			// 记录日志字段
			pData, _ := context.Get("PostJsonData")
			gParams, _ := context.Get("GetParamData")
			vv, _ := context.Get("ResponseData")
			//cid, _ := context.Get("cid")
			logFields := map[string]interface{}{
				"context":        fmt.Sprintf("%p", &context),
				"start_time":     EagoTime.DateTimeF4ByNow(startTime),
				"end_time":       EagoTime.DateTimeF4ByNow(endTime),
				"latency_time":   fmt.Sprintf("%.4f", latencyTime) + " s",
				"status_code":    context.Writer.Status(),
				"client_ip":      context.ClientIP(),
				"req_proto":      context.Request.Proto,
				"req_method":     context.Request.Method,
				"req_uri":        context.Request.RequestURI,
				"req_headers":    context.Request.Header,
				"req_post_json":  pData,
				"req_get_params": gParams,
				"resp_header":    context.Writer.Header(),
				"resp_body":      vv,
			}
			// 转成json字符串
			logFieldJson, _ := json.Marshal(logFields)
			logFieldJsonString := string(logFieldJson)
			// 写日志行
			log.Println(logFieldJsonString)
		}(context, startTime, endTime)

	}
}

func Logger2FileLight() gin.HandlerFunc {
	return func(context *gin.Context) {
		// 开始时间
		startTime := time.Now()
		// 处理请求
		context.Next()
		// 结束时间
		endTime := time.Now()
		// 异步处理log到文件
		go func(context *gin.Context, startTime time.Time, endTime time.Time) {
			// 执行时间
			sf, _ := strconv.ParseFloat(startTime.Format("150405.0000"), 64)
			ef, _ := strconv.ParseFloat(endTime.Format("150405.0000"), 64)
			latencyTime := ef - sf

			filePath := "logs/access/" + EagoTime.NowYearStr() + "/" + EagoTime.NowMonthStr() + "/"

			fileName := filePath + EagoTime.NowDayStr() + ".log"

			if !EagoFile.ExistPath(filePath) {
				_ = os.MkdirAll(filePath, os.ModePerm)
			}
			if !EagoFile.ExistsFile(fileName) {
				_ = EagoFile.CreateFile(fileName)
			}
			f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
			if err != nil {
				fmt.Println(err)
			}
			// 是否同时输出到控制台
			if config.AppConfig.Logger.Logger2Console {
				log.SetOutput(io.MultiWriter(os.Stderr, f))
			} else {
				log.SetOutput(f)
			}
			// 屏蔽默认前缀时间
			log.SetFlags(0)

			logFields := map[string]interface{}{
				"context":      fmt.Sprintf("%p", &context),
				"start_time":   EagoTime.DateTimeF4ByNow(startTime),
				"end_time":     EagoTime.DateTimeF4ByNow(endTime),
				"latency_time": fmt.Sprintf("%.4f", latencyTime) + " s",
				"status_code":  context.Writer.Status(),
				"client_ip":    context.ClientIP(),
				"req_proto":    context.Request.Proto,
				"req_method":   context.Request.Method,
				"req_uri":      context.Request.RequestURI,
				"req_headers":  context.Request.Header,
				"resp_header":  context.Writer.Header(),
				"req_params":   context.Request.URL.Query(),
			}
			// 转成json字符串
			logFieldJson, _ := json.Marshal(logFields)
			logFieldJsonString := string(logFieldJson)
			// 写日志行
			log.Println(logFieldJsonString)
		}(context, startTime, endTime)

	}
}

// 日志记录到 MQ
func LoggerToMQ() gin.HandlerFunc {
	return func(context *gin.Context) {
	}
}
