package resp

type HttpStatus []interface{}

var (
	SUCCESS                     = HttpStatus{0, "成功"}
	ErrorAuthNoToken            = HttpStatus{6000, "没有Token"}
	ErrorAuthCheckTokenFail     = HttpStatus{6666, "无效Token"}
	ErrorAuthCheckTokenTimeout  = HttpStatus{6667, "Token已失效"}
	ParamsMiss                  = HttpStatus{7000, "参数错误"}
	BusinessProcessingException = HttpStatus{9000, "业务处理异常"}
	ManagerLoginFail            = HttpStatus{6668, "登录失败"}
)

func ParseStatusCode(ss HttpStatus) int {
	return ss[0].(int)
}

func ParseStatusMsg(ss HttpStatus) string {
	return ss[1].(string)
}

func ParseStatusInfo(ss HttpStatus) (int, string) {
	return ss[0].(int), ss[1].(string)
}
