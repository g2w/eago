package vld

import (
	"gitee.com/g2w/eago/core/http/resp"
	"github.com/gin-gonic/gin"
)

type VInterface interface {
	ValidateV2(ctx *gin.Context, params resp.PData, structType interface{})
	AddRule() *Validator
}

type String string
type Number float64
type Array  []interface{}
type MapInt map[int64]interface{}
type MapStr map[string]interface{}

type Validator struct {
	rules map[string]interface{}
}


