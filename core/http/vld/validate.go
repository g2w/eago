package vld

import (
	"fmt"
	"gitee.com/g2w/eago/core/http/resp"
	"github.com/gin-gonic/gin"
	"reflect"
)

func ValidateV1(ctx *gin.Context, structType interface{}, params resp.PData) interface{} {
	// 反射结构体 value
	p := reflect.ValueOf(structType)
	// 得到元素
	v := p.Elem()
	// 得到元素内容
	tt := v.Type()

	// 遍历结构体 全部元素
	for i := 0; i < tt.NumField(); i++ {
		// 取得元素字段
		f := tt.Field(i)
		// 获取字段标签
		j := f.Tag.Get("json")
		b := f.Tag.Get("binding")

		// 获取字段对应的 参数值
		val := params[j]

		// 条件判断 是否必选参数
		if val != nil && b == "required" {
			// 字段类型 是否切片数据类型
			if f.Type.Kind() == reflect.Slice {
				// 转换为 ids 切片类型
				var ids []float64
				for _, id := range val.([]interface{}) {
					ids = append(ids, id.(float64))
				}
				// 设置结构体元素值
				v.FieldByName(f.Name).Set(reflect.ValueOf(ids))
			} else {

				// 判断参数类型和结构体字段类型是否一致
				pType := reflect.TypeOf(val)
				vType := v.FieldByName(f.Name).Type()
				if pType != vType {
					// fmt.Println("类型不一致：", reflect.TypeOf(val), v.FieldByName(f.Name).Type())
					// 字段类型错误 抛出异常
					comment := f.Tag.Get("comment")
					var errInfo = comment + "(" + j + ") 参数类型错误,需要(" + vType.String() + "),传入了(" + pType.String() + ")"
					ctx.Set("ParamsMissInfo", errInfo)
					panic(resp.ParamsMiss)
				}
				// 设置
				v.FieldByName(f.Name).Set(reflect.ValueOf(val))
			}
		} else {
			// 必选字段值不存在 抛出异常
			comment := f.Tag.Get("comment")
			ctx.Set("ParamsMissInfo", comment+"("+j+") 参数缺失~")
			panic(resp.ParamsMiss)
		}
	}
	// 返回结构体类型的数据
	return structType
}

func ValidateV2(ctx *gin.Context, params resp.PData, structType interface{}) {
	// 反射结构体 value // 得到元素 // 得到元素内容
	a1 := reflect.ValueOf(structType)
	a2 := a1.Elem()
	a3 := a2.Type()
	// 遍历结构体 全部元素
	for i := 0; i < a3.NumField(); i++ {
		fieldVal := a3.Field(i)
		pName := fieldVal.Tag.Get("json")
		isRequired := fieldVal.Tag.Get("binding")
		pData := params[pName]
		// 存在必选参数 校验
		if pData != nil && isRequired == "required" {
			// 类型校验
			fieldType := fieldVal.Type.Kind().String()
			pType := reflect.TypeOf(pData).Kind().String()
			if fieldType == pType {
				structFieldValue := a2.FieldByName(a3.Field(i).Name)
				fieldType := structFieldValue.Type()
				val := reflect.ValueOf(pData)
				structFieldValue.Set(val.Convert(fieldType))
			} else {
				// 字段类型错误 抛出异常
				comment := fieldVal.Tag.Get("comment")
				errInfo := comment + "(" + pName + ") 参数类型错误,需要(" + fieldType + "),传入了(" + pType + ")"
				ctx.Set("ParamsMissInfo", errInfo)
				panic(resp.ParamsMiss)
			}
		} else {
			// 必选字段值不存在 抛出异常
			comment := fieldVal.Tag.Get("comment")
			ctx.Set("ParamsMissInfo", comment+"("+pName+") 参数缺失~")
			panic(resp.ParamsMiss)
		}
	}
}

func checkP(p interface{}, f *reflect.StructField, te *reflect.Value) {
	pType := reflect.TypeOf(p)
	if pType == f.Type {
		te.FieldByName(f.Name).Set(reflect.ValueOf(p))
		fmt.Println(p, pType, f.Type, "ok")
	} else {
		vType := te.FieldByName(f.Name).Type()
		if pType == reflect.TypeOf([]interface{}{}) {
			if vType == reflect.TypeOf([]float64{}) {
				// 转换为 ids 切片类型
				var es []float64
				for _, e := range p.([]interface{}) {
					idType := reflect.TypeOf(e)
					if idType == reflect.TypeOf(float64(1)) {
						es = append(es, e.(float64))
					}
				}
				// 设置结构体元素值
				te.FieldByName(f.Name).Set(reflect.ValueOf(es))
			}
			if vType == reflect.TypeOf([]string{}) {
				// 转换为 string 切片类型
				var es []string
				for _, e := range p.([]interface{}) {
					idType := reflect.TypeOf(e)
					if idType == reflect.TypeOf("string") {
						es = append(es, e.(string))
					}
				}
				// 设置结构体元素值
				te.FieldByName(f.Name).Set(reflect.ValueOf(es))
			}

			fmt.Println(p, pType, vType)
		}
		if pType == reflect.TypeOf(map[string]interface{}{}) {
			fmt.Println(p, pType, vType)
		}
	}
}

// 切片
func v1(p interface{}, f *reflect.StructField, te *reflect.Value) {
	ps := p.([]interface{})
	//fs := f.([]interface{})
	for _, v := range ps {
		//fmt.Println(k, v)
		vType := reflect.TypeOf(v)
		//fType := reflect.TypeOf(fs[k])
		fmt.Println("v1===>", vType)
		checkP(v, f, te)
	}
}

// map
func v2(p interface{}, f *reflect.StructField, te *reflect.Value) {
	m := p.(map[string]interface{})
	//fm := f.(map[string]interface{})
	for _, v := range m {
		//fmt.Println(k, v)
		vType := reflect.TypeOf(v)
		//fType := reflect.TypeOf(fm[k])
		fmt.Println("v2===>", vType)
		checkP(v, f, te)
	}
}

func v3(p interface{}, f *reflect.StructField, te *reflect.Value) {
	fmt.Println("v3")
}

func v4(p interface{}, f *reflect.StructField, te *reflect.Value) {
	fmt.Println("v4")
}
