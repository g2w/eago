package websokets

import (
	"gitee.com/g2w/eago/core/utils/EagoTime"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	"net/http"
)

//设置websocket
//CheckOrigin防止跨站点的请求伪造
var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//websocket实现
func GetCurrentTime(c *gin.Context) {
	//升级get请求为webSocket协议
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		return
	}
	defer ws.Close() //返回前关闭
	for {
		//读取ws中的数据
		mt, _, err := ws.ReadMessage()
		if err != nil {
			break
		}
		//写入ws数据
		nowT := EagoTime.NowDateTimeStr()
		err = ws.WriteMessage(mt, []byte(nowT))
		if err != nil {
			break
		}
	}
}
