package params

import iValidator "gitee.com/g2w/eago/core/http/vld"

// 参数验证类型引入
type VString 	iValidator.String
type VNumber 	iValidator.Number
type VArray 	iValidator.Array
type VMapIntKey iValidator.MapInt
type VMapStrKey iValidator.MapStr
