package params

type ManagerLoginCheck struct {
	Account  VString `json:"account" binding:"required" comment:"账号"`
	Password VString `json:"password" binding:"required" comment:"密码"`
}
