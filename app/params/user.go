package params

// 参数结构定义
type Login struct {
	Ids     VArray  `json:"ids" binding:"required" comment:"id数组"`
	Filter  VString `json:"filter" binding:"required" comment:"筛选(-1全部 1正在进行中 0已结束 2未开始)"`
	PageNum VString `json:"page_num" binding:"required" comment:"页码"`
	PagePer VString `json:"page_per" binding:"required" comment:"每页数"`
}
