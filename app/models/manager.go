package models

type Manager struct {
	BaseModel
	ManagerName     string `gorm:"not null;comment:名字"`
	ManagerAccount  string `gorm:"not null;comment:账号"`
	ManagerPassword string `gorm:"not null;comment:密码"`
	RoleId          int8   `gorm:"not null;default:1;comment:角色ID（1普通 0超管）"`
	OpenStatus      int8   `gorm:"not null;default:1;comment:开启状态（1开启 0关闭）"`
}
