package api

import (
	"gitee.com/g2w/eago/app/handlers"
	"gitee.com/g2w/eago/app/params"
	"gitee.com/g2w/eago/core/http/resp"
	"github.com/gin-gonic/gin"
)

func Login(ctx *gin.Context) {
	var p params.Login
	handlers.GetAllParamsWithValidate(ctx, &p)
	handlers.EchoJSON(ctx, resp.SUCCESS, resp.RData{"params": p})
}
