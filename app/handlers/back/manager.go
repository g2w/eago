package back

import (
	"gitee.com/g2w/eago/app/handlers"
	"gitee.com/g2w/eago/app/params"
	"gitee.com/g2w/eago/core/http/resp"
	"github.com/gin-gonic/gin"
)

func LoginCheck(ctx *gin.Context) {
	var p params.ManagerLoginCheck
	handlers.GetAllParamsWithValidate(ctx, &p)
	rData := resp.RData{
		"params": p,
	}
	handlers.EchoJSON(ctx, resp.SUCCESS, rData)
}
