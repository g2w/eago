package handlers

import (
	"gitee.com/g2w/eago/core/http/resp"
	"gitee.com/g2w/eago/core/http/vld"
	"gitee.com/g2w/eago/core/utils/EagoLog"
	"github.com/gin-gonic/gin"
)

// 统一输出
func EchoJSON(ctx *gin.Context, statusCode resp.HttpStatus, result resp.RData) {
	code, msg := resp.ParseStatusInfo(statusCode)
	ResponseData := gin.H{"code": code, "msg": msg, "result": result}
	ctx.JSON(200, ResponseData)
}

// 获取GET和POST参数合并 带校验
func GetAllParamsWithValidate(ctx *gin.Context, structType interface{}) {

	getParams := GetParams(ctx)
	postJsonParams := GetRawJson(ctx)

	var params = make(map[string]interface{})
	for i, v := range getParams {
		params[i] = v
	}
	for ii, vv := range postJsonParams {
		params[ii] = vv
	}
	// 参数校验后返回目标类型
	vld.ValidateV2(ctx, params, structType)
}

// 获取GET和POST参数合并
func GetAllParams(ctx *gin.Context) resp.PData {

	getParams := GetParams(ctx)
	postJsonParams := GetRawJson(ctx)

	var params = make(map[string]interface{})
	for i, v := range getParams {
		params[i] = v
	}
	for ii, vv := range postJsonParams {
		params[ii] = vv
	}
	//ctx.Set("ParamsData", params)
	return params
}

// 获取GET参数
func GetParams(ctx *gin.Context) resp.PData {
	values := ctx.Request.URL.Query()
	params := make(resp.PData)
	for k, v := range values {
		params[k] = v[0]
	}
	return params
}

// 获取POST raw json 数据
func GetRawJson(ctx *gin.Context) resp.PData {
	var pJson resp.PData
	_ = ctx.ShouldBindJSON(&pJson)
	return pJson
}

func ThrowException(code resp.HttpStatus, data EagoLog.DataType) {
	if data != nil {
		EagoLog.ExceptionLog2File(data)
	}
	panic(code)
}
